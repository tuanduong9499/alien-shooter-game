export class CollisionDetector {
  static init() {
    this.tag1 = [];
    this.tag2 = [];
  }

  static addTag1(collider) {
    this.tag1.push(collider);
  }

  static addTag2(collider) {
    this.tag2.push(collider);
  }

  static update() {
    this.tag1.forEach((collider1, index1) => {
      this.tag2.forEach((collider2, index2) => {
        if(collider1.enable && collider2.enable){
          if (this.checkCollide(collider1, collider2)) {
            collider1.onCollide();
            collider2.onCollide();
          } else if (!collider1.enable) {
            this.tag1.splice(index1, 1);
          } else if (!collider2.enable) {
            this.tag2.splice(index2, 1);
          }
        }
      });
    });
  }

  static checkCollide(collider1, collider2) {

    let pos1 = collider1.getBounds();
    let pos2 = collider2.getBounds();

    return this.checkAabb(
      pos1.x,
      pos1.y,
      pos1.width,
      pos1.height,
      pos2.x,
      pos2.y,
      pos2.width,
      pos2.height
    );
  }

  static checkAabb(x1, y1, w1, h1, x2, y2, w2, h2) {
    return x1 < x2 + w2 && x1 + w1 > x2 && y1 < y2 + h2 && y1 + h1 > y2;
  }
}