import { Container, ParticleContainer, Sprite, Texture, TilingSprite } from "pixi.js";
import { EnemyController } from "../objects/enemy/enemyController";
import { Ship } from "../objects/ship/ship";

export class PlayScene extends Container {
  constructor(){
    super();
    this.initGamePlay();

  }

  initGamePlay(){
    let texture = Texture.from("../../assets/background7.png");
    this.background = new Sprite(texture);
    this.background = new TilingSprite(
      texture,
      window.innerWidth,
      window.innerHeight
    );
    this.addChild(this.background);
    this.initShip();
    this.initEnemies();
  }

  initShip(){
    this.ship = new Ship(this);
    this.addChild(this.ship);
  }

  initEnemies(){
    this.enemies = new EnemyController();
    this.addChild(this.enemies);
  }

  update(dt){
    this.background.tilePosition.y += 2;
    this.ship.update(dt);
    this.enemies.update(dt);
  }

}