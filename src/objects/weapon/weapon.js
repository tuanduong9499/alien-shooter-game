import { Container } from "pixi.js";
import { CollisionDetector } from "../../collisionDetector/collisionDetector";
import { ColliderEvent } from "../collider/collider";

export class Weapon extends Container {
  constructor(config = defaultWeaponConfig) {
    super();
    this.bullets = [];
    this.bulletParent = config.bulletParent;
    this.velocityX = config.velocityX;
    this.velocityY = config.velocityY;
    
  }
  addBullet(bullet){
    this.bullets.push(bullet);
  }

  update(){
    this.bullets.forEach((bullet, index) => {
      bullet.children[0].on(ColliderEvent.collider, () => {
        bullet.visible = false;
      })
      if(bullet){
        bullet.x += this.velocityX;
        bullet.y += this.velocityY;
        //destroy bullet when outside of the screen
        if(bullet.y <= 0 ){
          this.bullets.splice(index, 1);
          this.bulletParent.destroyBullet(bullet);
        }
      }
    })
  }
}
export const defaultWeaponConfig = {
  velocityX : 0,
  velocityY : 0
};