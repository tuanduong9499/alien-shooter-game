import { AnimatedSprite, Assets, Container, Sprite, Texture } from "pixi.js";
import { GameConstant } from "../../../gameConstants";
import { CollisionDetector } from "../../collisionDetector/collisionDetector";
import { Collider, ColliderEvent } from "../collider/collider";

export class BulletEnemy extends Container {
  constructor(x, y){
    super();
    this.x = x;
    this.y = y;
    Assets.load("//assets/spriteSheet/enemies/bulletEnemy.json").then(() => {
      const frames = [];
      for (let i = 1; i <= 20; i++) {
        const val = i <= 20 ? `${i}` : i;
        frames.push(Texture.from(`sunburn_${val}.png`));
      }
      const anim = new AnimatedSprite(frames);
      anim.anchor.set(0.5);
      anim.animationSpeed = 2;
      anim.play();
      this.addChild(anim);
    });
    this.config = {
      width : 30,
      height : 30,
      name : "bulletEnemy"
    }
    this.collider = new Collider();
    this.collider.create(this.config);
    this.addChild(this.collider);
    this.collider.visible = GameConstant.DEBUG_COLLIDER;
    
    CollisionDetector.addTag2(this.collider);
  }

}