import { Container } from "pixi.js";
import { CollisionDetector } from "../../collisionDetector/collisionDetector";
import { BulletEnemy } from "../bulletEnemy/bulletEnemy";
import { ColliderEvent } from "../collider/collider";
import { Enemy } from "./enemy";
import { EnemyFire } from "./enemyFire";
export class EnemyController extends Container {
  constructor() {
    super();
    this.enemies = [];
    this.enemiesDie = [];
    this.bullets = [];
    this.dt = 0;
    this.create();  
    this.onCollide();
    // this.y = -500;
    // this.interval = setInterval(() => {
    //   this.y += 1;
    //   if(this.y >= 0){
    //     clearInterval(this.interval);
    //   }
    // },10);
  }

  create() {
    this.enemiesConfig = {
      row: 5,
      column: 2,
      marginLeft: 100,
      marginTop: 100,
      distanceX: 100,
      distanceY: 100,
    };
    for (let i = 0; i < this.enemiesConfig.row; i++) {
      for (let j = 0; j < this.enemiesConfig.column; j++) {
        let enemyX =
          i * this.enemiesConfig.distanceX + this.enemiesConfig.marginLeft;
        let enemyY =
          j * this.enemiesConfig.distanceY + this.enemiesConfig.marginTop;
          let enemy = new Enemy(enemyX, enemyY);
          this.addChild(enemy)
          this.enemies.push(enemy);
      }
    }
  } 

  onCollide(){
    this.enemies.forEach((enemy, index) => {
      enemy.collider.on(ColliderEvent.collider, () => {
        enemy.health -= 1;
        if (enemy.health == 5) {
          let enemyFire = new EnemyFire();
          enemy.addChild(enemyFire);
          let bulletEnemy = new BulletEnemy(enemy.x, enemy.y);
          this.addChild(bulletEnemy);
          this.bullets.push(bulletEnemy);
          
        } else if (enemy.health <= 0) {
          enemy.collider.enable = false;
          enemy.destroy();
        }
      })
    })

  }

  update(dt) {
    this.bullets.forEach((bullet, index) => {
      bullet.y += 5;
      if(bullet.y >= 700){
        this.bullets.splice(index, 1)
        bullet.destroy()
      }
    });
  }
}