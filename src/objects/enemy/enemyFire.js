import { AnimatedSprite, Assets, Container, Texture } from "pixi.js";

export class EnemyFire extends Container {
  constructor() {
    super();
    Assets.load(
      "../../../assets/spriteSheet/enemies/enemySpriteSheet.json"
    ).then(() => {
      const frames = [];
      for (let i = 1; i <= 9; i++) {
        const val = i <= 9 ? `${i}` : i;
        frames.push(Texture.from(`Monster_Fire_${val}.png`));
      }
      const anim = new AnimatedSprite(frames);
      anim.anchor.set(0.5);
      anim.animationSpeed = 0.5;
      anim.play();
      this.addChild(anim);
    });
  }
}
