import { AnimatedSprite, Assets, Container, Loader, Sprite, Texture } from "pixi.js";
import { GameConstant } from "../../../gameConstants";
import { CollisionDetector } from "../../collisionDetector/collisionDetector";
import { Collider } from "../collider/collider";

export class Enemy extends Container {
  constructor(x, y){
    super();
    this.health = 8;
    this.x = x;
    this.y = y;

    Assets.load(
      "../../../assets/spriteSheet/enemies/enemySpriteSheet2.json"
    ).then(() => {
      const frames = [];
      for (let i = 1; i <= 2; i++) {
        const val = i <= 2 ? `${i}` : i;
        frames.push(Texture.from(`Monster_Fire_${val}.png`));
      }
      const anim = new AnimatedSprite(frames);
      anim.anchor.set(0.5);
      anim.animationSpeed = 0.1;
      anim.play();
      this.addChild(anim);
    });

    this.collider = new Collider();
    this.config = {
      width: 60,
      height: 60,
      name: "enemy",
    };
    this.collider.create(this.config);
    this.collider.visible = GameConstant.DEBUG_COLLIDER;
    this.addChild(this.collider);
    CollisionDetector.addTag2(this.collider)
  }
}