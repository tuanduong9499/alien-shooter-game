import { Container, Sprite, Texture } from "pixi.js";
import { GameConstant } from "../../../gameConstants";
import { CollisionDetector } from "../../collisionDetector/collisionDetector";
import { BulletShip } from "../bulletShip/bulletShip";
import { Collider, ColliderEvent } from "../collider/collider";
import { Weapon } from "../weapon/weapon";
import { ShipFire } from "./shipFire";

export class Ship extends Container{
  constructor(parent){
    super();
    this.parent = parent;
    this.create();
    this.initShipFire();
    this.move();
    this.initWeapon(parent);
    this.dt = 0;
  }

  create(){
    this.shipConfig = {
      width : 70,
      height : 70
    }
    let texture = Texture.from("../../assets/ship.png");
    this.ship = new Sprite(texture);
    this.ship.anchor.set(0.5, 0.5);
    this.ship.scale.set(0.5);
    let collider = new Collider();
    collider.visible = GameConstant.DEBUG_COLLIDER;
    collider.create(this.shipConfig);
    this.ship.addChild(collider);
    this.addChild(this.ship);
    // CollisionDetector.addTag1(collider);

    // collider.on(ColliderEvent.collider, () => {
    //   collider.enable = false;
    //   this.visible = false;
    // })
  }

  initShipFire(){
    this.shipFire_right = new ShipFire(110, 110);
    this.shipFire_left = new ShipFire(-110, 110)
    this.ship.addChild(this.shipFire_right, this.shipFire_left)
  }

  move(){
    document.body.addEventListener("mousemove", (e) => {
      this.x = e.clientX;
      this.y = e.clientY;
    });
  }

  initWeapon(parent){
    this.bullet = new BulletShip();
    parent.addChild(this.bullet);
    this.weapon = new Weapon({
      bulletParent : this.bullet,
      velocityX: 0,
      velocityY : -15,
      
    });
    this.addChild(this.weapon);

  }

  update(dt){
    this.dt += dt;
    this.weapon.update(dt);
    if(Math.round(this.dt) % 10 == 0){
      let bullet = this.bullet.spawn(this.getGlobalPosition());
      this.weapon.addBullet(bullet);
    }
  }

}