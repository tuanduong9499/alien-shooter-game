import { AnimatedSprite, Assets, Container, Texture } from "pixi.js";

export class ShipFire extends Container {
  constructor(x, y){
    super();
     Assets.load("//assets/spriteSheet/ship/shipFire.json").then(() => {
      const frames = [];
      for (let i = 1; i <= 15; i++) {
        const val = i <= 15 ? `${i}` : i;
        frames.push(Texture.from(`shipFire_${val}.png`));
      }
      const anim = new AnimatedSprite(frames);
      anim.anchor.set(0.5);
      anim.animationSpeed = 2;
      anim.play();
      this.addChild(anim);
     });
     this.scale.set(2);
     this.x = x;
     this.y = y;
     this.rotation = 4.7;
  }
}