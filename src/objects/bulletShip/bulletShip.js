import { Container, Sprite, Texture } from "pixi.js";
import { CollisionDetector } from "../../collisionDetector/collisionDetector";
import { Collider, ColliderEvent } from "../collider/collider";

export class BulletShip extends Container {
  constructor(){
    super();
  }
  create(){
    let texture = Texture.from("../../assets/bulletShip.png")
    this.bullet = new Sprite(texture);
    this.bullet.anchor.set(0.5, 0.5);
    this.bullet.scale.set(0.7)
    this.config = {
      width : 20,
      height : 50,
      name : "bulletShip"
    }
    let collider = new Collider();
    collider.create(this.config);
    collider.visible = false;
    this.bullet.addChild(collider);
    this.addChild(this.bullet);
    CollisionDetector.addTag1(collider);
    collider.on(ColliderEvent.collider, () => {
        collider.enable = false;
    });
    return this.bullet;
  }

  spawn(position){
    let bullet = this.create();
    bullet.x = position.x;
    bullet.y = position.y;
    return bullet;
  }

  destroyBullet(bullet){
    bullet.destroy();
  }
}