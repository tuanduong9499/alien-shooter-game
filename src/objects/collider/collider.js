import { Container, Sprite, Texture } from "pixi.js";

export const ColliderEvent = {
  collider : "collider" 
}
export class Collider extends Container {
  constructor(){
    super();
  }

  create(config = colliderConfigDefault){
    let texture = Texture.from("../../../assets/spr_white.png");
    this.collider = new Sprite(texture);
    this.collider.anchor.set(0.5);
    this.collider.width = config.width;
    this.collider.height = config.height;
    this.collider.name = config.name;
    this.enable = true;
    this.addChild(this.collider);
  }

  onCollide(){
    this.emit(ColliderEvent.collider);
  }
}

export const colliderConfigDefault = Object.freeze({
  width : 10,
  height : 10,
  name : ""
});
