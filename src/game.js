import { Application, Container } from "pixi.js";
import { CollisionDetector } from "./collisionDetector/collisionDetector";
import { PlayScene } from "./scenes/playScene";

export default class Game {
  constructor(){
    this.app = new Application({
      width: window.innerWidth,
      height : window.innerHeight
    });
    CollisionDetector.init();
    document.body.appendChild(this.app.view);
    this.app.ticker.add(this.update, this);
    this.initScene();
  }
  initScene(){
    this.playScene = new PlayScene();
    this.app.stage.addChild(this.playScene);
  }

  update(dt){
    CollisionDetector.update()
    this.playScene.update(dt)
  }
}