export const GameConstant = Object.freeze({
  GAME_WIDTH: 1280,
  GAME_HEIGHT: 720,

  DEBUG_COLLIDER: false,
});
